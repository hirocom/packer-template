# Packer-template
Packerテンプレート
* centos 7.2
* chef 12.12.13

## 実行環境

* [virtualbox](https://www.virtualbox.org/)
* [vagrant](https://www.vagrantup.com/)
* [packer](https://www.packer.io/)

## インストール方法
```
$ cd 作業ディレクトリ
$ git clone git@bitbucket.org:hirocom/packer-template.git
```

## 使い方

#####boxの作成
```
$ cd 作業ディレクトリ/packer-template
$ packer build -only=virtualbox-iso packer.json
```

#####Vagrant up
```
$ vagrant box add centos72 作業ディレクトリ/packer-template/builds/centOS-7.2-x86_64-minimal.chefdk.box
$ vagrant init centos72
$ vagrant up
```